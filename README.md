[Watch Demo on YouTube](https://www.youtube.com/watch?v=wOtbknmv1SA)

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/wOtbknmv1SA/0.jpg)](https://www.youtube.com/watch?v=wOtbknmv1SA)

This video shows the [DeepPortrait](https://gitlab.com/UteOK/deepportrait) project which uses DeepStyle as backend.


# DeepStyle Project

VERSION 1.1

HDM Stuttgart
Medieninformatik

Semester Projekt 2018/2019

Authorin       :  Ute Orner-Klaiber
Betreuer       :  Prof. Dr.-Ing. Johannes Maucher, Johannes Theodoridis


