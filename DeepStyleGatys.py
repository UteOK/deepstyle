
# coding: utf-8

# <img src="jupyterbilder/22doku.jpg"  width="400">
# 
# 
# * Autor: Ute Orner-Klaiber
# * Betreuer: Prof. Johannes Maucher, Johannes Theodoridis
# * Zeitraum: Wintersemester 18/19
# 
# # 1. Einführung:
# 
# **„Neural Style Transfer“ beschreibt den Vorgang des Übertragens des Stiles eines Bildes auf den Inhalt eines anderen Bildes mit Hilfe von „Machine Learning“ Algorithmen. Hierzu wird ein Content-Bild und ein Style-Bild benötigt. Ist das Style-Bild z.B. ein Gemälde von Vincent van Gogh und das Content-Bild ein Foto vom Eiffelturm, dann erzeugt „Neural Style Transfer“ ein Bild des Eiffelturms im Stil eines Vincent van Gogh Gemäldes. In diesem Projekt wurden mehrere tiefe neuronale Netzwerke mit Python implementiert, um verschiedene „Neural Style Transfer“ Verfahren durchzuführen und um diese im Hinblick auf Performance und Qualität zu vergleichen. Der Schwerpunkt lag dabei auf der Performance. Die zwei Algorithmen sind in ausgewählten wissenschaftlichen Publikationen beschrieben.**
# 
# 
# 
# ## 1.1. Beispiel:
# <img src="jupyterbilder/gbeispiel.jpg">
# 
# ## 1.2. Prinzip:
# Als Grundlage für den Neural Style Transfer wird ein vortrainiertes Model benutzt. Dieses Model ist ein CNN das darauf spezialisiert ist, Objekte zu erkennen (z.B.VGG). In den niedrigen Layern werden Linien und Formen erkannt und in den höheren Layern werden Objekte und komplexere Formen erkannt. Diese Eigenschaft wird für den Neural Style Transfer genutzt. Die fully connected layers am Schluss des CNNs werden weggelassen, da kein Objekt erkannt werden muss, sondern ein Bild der Output ist. Die entscheidende Idee ist es, zwei Costfunctions zu nutzen. Eine repräsentiert den Style (Textur, Farbe vom Style Image) und eine repräsentiert den Inhalt (Struktur und Formen vom Content Image). Da beide Costfunctions minimiert werden, kommt ein guter Kompromiss zwischen beiden Bildern heraus. 
# 
# C=Content Image, G=Generated Image, S=Style Image
# - Content-cost-Funktion: $L_{content}(C,G)$ 
# 
# 
# 
# - Style-cost-Funktion: $L_{style}(S,G)$
# 
# 
# 
# - Beide Funktionen zusammengesetzt: $L(G) = \alpha J_{content}(C,G) + \beta J_{style}(S,G)$,   $\alpha$ und $\beta$ sind Hyperparameter
# 
# 
# Die Formeln werden im Folgenden genauer erklärt und implementiert. Die Werte der Pixel (zwischen 0 und 255) von dem Generated Image wird durch den gradient descent so verändert, dass die beiden Costfunctions minimiert werden. Das CNN sucht also eine Pixelzusammenstellung, so dass beide Costfunctions sehr gering werden. Dadurch entsteht das gewünschte Bild, das eine Mischung aus Style- und Content Image ist.
# 
# 
# 
# 

# # 2. A Neural Algorithm of Artistic Style
# 
# 
# * Autoren: Leon A. Gatys, Alexander S. Ecker, Matthias Bethge
# * Erscheinungsdatum: 02.09.2015
# 
# Eckdaten: Als Grundlage wird VGG 19 verwendet. 16 convolutional und 5 pooling layer werden implementiert und die fully connected layer des VGGs werden weggelassen. Außerdem wird nicht max-pooling benutzt, sondern average-pooling, dadurch wird das Bildergebnis und die Performance des gradient descent gesteigert. Der folgende Code wurde auf Basis des Papers <br>
# [1] Gatys, Leon A., Alexander S. Ecker, and Matthias Bethge. "A neural algorithm of artistic style." arXiv preprint arXiv:1508.06576 (2015) <br>selbst implementiert.
# 
# ### Code:

# In[1]:


#Testen ob tensorflow und keras funktionieren
try:
    import keras
    import tensorflow as tf
    from tensorflow.python.client import device_lib
    from tensorflow.python.keras.preprocessing import image as K_image
    from tensorflow.python.keras import models 
    from tensorflow.python.keras import layers
    from tensorflow.python.keras import losses
    from tensorflow.python.keras import backend as K
except:
    print("Fehler beim Keras-TF laden")
    sys.exit(-1)
    
tf_version = tf.__version__

if (tf_version != "1.10.0"):
    print(str(tf_version) + " ist falsche TF Version")
else:
    print("TF Version " + tf_version)


# In[2]:


#GPU auf JARVIS
import os
jarvis_gpu = 3
os.environ["CUDA_VISIBLE_DEVICES"] = str(jarvis_gpu)
print("Reserviere GPU: " + str(os.environ["CUDA_VISIBLE_DEVICES"]))

from tensorflow.python.client import device_lib
try:
    #print("Verfügbare Devices")
    devices = device_lib.list_local_devices()
    gpu_bekommen = False
    for dev in devices:
        #print(dev)
        if str(dev).find("GPU"):
            gpu_bekommen = True

    if gpu_bekommen == False:
        print("Fehler beim reservieren der GPU " + str(jarvis_gpu))
        sys.exit(0)
    else:
        print("Jarvis GPU " + str(jarvis_gpu) + " erfolgreich reserviert")
        print("---------------------")
except:
        print("Fehler beim reservieren der GPU " + str(jarvis_gpu))
        quit()
        


# In[3]:


import sys
from matplotlib.pyplot import imshow
import matplotlib.pyplot as plt

if __name__ == "__main__":
    get_ipython().run_line_magic('matplotlib', 'inline')
    
import scipy.io
import scipy.misc
from PIL import Image
import functools
import time
import numpy as np
import time


# In[4]:


#Globale Variablen
image_dim  = (1,224,224,3)
vgg_mean = np.array([103.939, 116.779, 123.68]).astype('uint8') #für VGG19
content_output_layer = ['conv2_1'] #relevante Layer für Content
style_output_layer = ['conv1_1','conv2_1','conv3_1','conv4_1','conv5_1'] #relevante Layer für den Style
wl = [0.1,0.1,0.1,0.1,0.6] #Gewichtung der Style Layer
learning_rate = 0.2 #im Paper keine Angabe über den Defaultwert der Learning-rate


#  ## 2.1. VGG 19 laden
# 
# VGG19 möchte das Inputimage in einer bestimmten Form haben, um es optimal verarbeiten zu können. Deshalb muss das Image speziell importiert und geladen werden. VGG19 verlangt 4 Dimensionen (1,Höhe in Pixel,Breite in Pixel,3) die erste ist die Anzahl der Batches, in unserem Fall ist das 1, weil es wird nur ein Bild reingegeben. Die zweite und dritte Dimension ist die Breite und Höhe unseres Bildes und die vierte Dimension sind die Channels, es sind 3, weil das Bild in RGB vorliegen muss. Als Orientierung wurde folgende Internetseite herangezogen: https://machinelearningmastery.com/use-pre-trained-vgg-model-classify-objects-photographs/. Als Defaultwert verarbeitet VGG19 224 x 224 große Bilder. Da hier die Fully Connected Layer nicht verwenden, könnte man hier auch andere Größen verwenden. Allerdings geht dann die Berechnung deutlich länger und da es in dem Paper keinen Hinweis auf eine andere Bildgröße gibt, wurden ebenfalls die Maße 224 x 224 Pixel verwendet.
# 
# Damit VGG19 die Bilder optimal verarbeiten kann, wird der Durchschnittswert der Pixel von dem jeweiligen Channel subtrahiert. Daher kommen die __vgg_mean__ Werte. (siehe globale Variablen)
# 
# __Würde man mit Keras das VGG19 Netzwerk laden, müssten vollgende Parameter eingestellt werden:__<br>
# __"include_top=False"__ wird verwendet, weil die Fully-Connected-Layer nicht geladen werden sollen. <br>
# __siehe__ [1]Seite 9 "...we do not use fully connected layers" <br>
# __"weights = 'imagenet' "__, ImageNet ist eine große Datenbank mit gelabelten Bildern, mit der vgg19 trainiert wurde. <br>
# __"pooling='avg' "__ wird verwendet, weil es bessere Ergebnisse liefert. <br>
# __siehe__ [1]Seite 9 "...average pooling improves the gradient flow" <br>
# __"layer.trainable=False"__ bedeutet, dass die Gewichte schon trainierte Werte haben und sie sollen nicht weiter trainiert werden
# 
# __siehe__ https://keras.io/applications/
# 
# Für diese Implementierung wurde das VGG19 Netz von Andrew Ng verwendet (hier vgg19_2 genannt), das von folgendem Link heruntergeladen wurde:<br>
# http://www.vlfeat.org/matconvnet/pretrained/

# Bildliche Darstellung des original VGG19 Netzwerks:
# 
# <img src="jupyterbilder/vgg19.jpg">
# 
# Quelle Bild: https://www.researchgate.net/figure/llustration-of-the-network-architecture-of-VGG-19-model-conv-means-convolution-FC-means_fig2_325137356

# In[5]:


#lädt das VGG19-Model
import vgg19_2
vgg = vgg19_2.load_vgg_model("imagenet-vgg-verydeep-19.mat",image_dim[1],image_dim[2])


# Die Funktion __load_image(pfad)__ bereitet die Bilder optimal auf VGG19 vor.

# In[6]:


def load_image(img):
    if isinstance(img, str):
        image = Image.open(img)
        image = image.convert(mode="RGB")
    elif isinstance(img,np.ndarray):
        if img.shape != image_dim[1:]:
            print("Input Array hat falsche Dimensions: " + str(img.shape) + " erwartet: " + str(image_dim[1:]))
        image = Image.fromarray(img, mode="RGB")
    else:
        print("Falscher Input für load image")
        quit()
                
    image = image.resize((image_dim[2],image_dim[1]), Image.ANTIALIAS)
    image = K_image.img_to_array(image)
    
    
    print("load image " + str(image.shape))
        
    assert(image.shape == (image_dim[1],image_dim[2],image_dim[3])) 
    image = np.expand_dims(image, axis=0) #fügt eine 1 als Länge der ersten Dimension an, 1=Anzahl der Daten (batch)
    image[:, :, 0] -= vgg_mean[0] #r
    image[:, :, 1] -= vgg_mean[1] #g
    image[:, :, 2] -= vgg_mean[2] #b
    image = np.clip(image, 0, 255).astype('uint8')

    return image 


# Hier wird das White Noise Image erstellt, dieses Image dient als Grundlage für das Generated Image. <br>
# __siehe__ [1]Seite 11, letzte Zeile, "we jointly minimise the distance of a white noise image from the content representation". Allerdings wird in dieser Anwendung als Startbild das Content Image benutzt und nicht ein White Noise Image, weil man dadurch mit weniger Iterationen zu einem guten Ergebnis kommt.

# In[7]:


white_noise = np.random.rand(image_dim[0],image_dim[1],image_dim[2],image_dim[3])


#  ## 2.2. Content Loss
# Die Aufgabe dieser Funktion ist es, das White Noise Image näher an das Content Image zu bringen. je kleiner das Ergebnis, umso ähnlicher ist das Generated Image dem Content Image. Sind $F$ und $P$ gleich, ist der Loss=0, dann sind die Bilder also exakt gleich.<br>Für den Content wird nur ein Layer von VGG19 verwendet und zwar der __Layer: Conv4_2__<br>
# __siehe__ [1]Seite 12 "we matched the content representation on layer ‘conv4_2’ "
# 
# Der Output nach diesem Layer wird für das Content Image __content_features__ genannt. In __content_features__ sind alle Informationen enthalten, die der Output des Layers conv4_2 enthält, nachdem man das Content Image durch VGG19 geschickt hat.
# __Content_features__ ist eine Konstante und wird nicht mehr verändert. Sie ist in $F$ gespeichert. $P$ enthält die __generated_features__ und beeinhalten den Output nachdem man das Generated Image durch VGG19 geschickt hat. $P$ ist keine Konstante und nähert sich durch "gradient descent" an $F$ an. Beim ersten Durchlauf ist das Generated Image ein White Noise Image. Die Werte der Pixel im White Noise Image verändern sich so, dass $L$ geringer wird, sie passen sich also dem Content Image an. Die Formel beschreibt die __Mittlere quadratische Abweichung__. 
# 
# $F$ = content_features (vom Content Image, Konstante)<br>
# $P$ = generated_features (vom Genereated Image, verändert sich)
# 
# $L_{content}(F, P) =  \frac{1}{2}\sum _{ \text{i,j}} (F^{(l)}_{ \text{i,j}} - P^{(l)}_{ \text{i,j}})^2 $ <br>
# 
# 
# __siehe__ [1]Seite 10

# In[8]:


#P=generated_features
#F=content_features


def losscontent(F, P):
    with tf.variable_scope('losscontent'):
        losscon = tf.multiply( 0.5 , tf.reduce_sum( tf.square(tf.subtract(F , P)) ) )    
    return losscon


#  ## 2.3. Gram-Matrix
# Der Style wird bestimmt, indem die Korrelation der Aktivierungen (Output-Features) der verschiedenen Channels (Filter) gemessen werden. Ein Channel ist z.B. für vertikale Linien zuständig und der andere Channel für Grüntöne. Kommt oft die grüne Farbe in Kombination mit vertikalen Linien vor, sind die Channels stark korreliert. Das wird in der Gram-Matrix abgebildet. Die Gram-Matrix ist immer quadratisch, weil die Anzahl der Spalten und die Anzahl der Zeilen der Anzahl der Channels des jeweiligen Layers entspricht. In diesem Fall haben wir fünf Layer die relevant für den Style sind, also fünf verschiedene Gram-Matrizen. Diese fünf Matrizen werden für das Style-Image und für das Generated-Image berechnet. Die Gram-Matrix wird berechnet, indem man die „aufgerollte Filter-Matrix“ transponiert und mit der ursprünglichen „aufgerollten Filter-Matrix“ multipliziert (siehe Abbildung). Ist die Korrelation hoch, sind die Werte der Gram-Matrix hoch. 
# 
# <img src="jupyterbilder/ggrammatrix.jpg">
# 
# Quelle Bild: Andrew Ng

# In[9]:


#Gram-Matrix Berechnung

def grammatrix(R,p="tensorflow"):
    if p=="no_tensorflow":
        m,fw,fh,fc=R.shape
        GM = np.zeros((fc,fc))
        for i in range(fw):
            for j in range(fh):
                    for k in range(fc):
                        for l in range(fc):
                            GM[k][l] += F[i][j][k]*F[i][j][l]
        return GM
    else:
        
        m,fw,fh,fc=R.shape
        with tf.variable_scope('grammatrix'):
            R=tf.reshape(tf.transpose(R),[fc,fw*fh]) #R ist jetzt zweidimensional
            dot = tf.tensordot(R,tf.transpose(R),axes=1)

        return dot


#  ## 2.4. Style Loss
# Durch diese Funktion wird das Generated Image immer näher an das Style Image angeglichen, dadurch entsteht der "Style" im Generated Image.<br>
# Für den Style werden 5 Layer von VGG19 verwendet und zwar die Layer:<br>
# __Layers: Conv1_1,Conv2_1,Conv3_1,Conv4_1,Conv5_1__<br>
# __siehe__ [1]Seite 12 "...and the style representations on layers ‘conv1 1’, ‘conv2 1’, ‘conv3 1’, ‘conv4 1’ and ‘conv5 1’"
# Die Style Rekonstruktion ist in diesen Layern sehr verschieden, bei den unteren Layern sind fast nur die Farben den Styles erkennbar und je höher die Layer werden, umso mehr erkennt man auch Strukturen/Texturen des Styles, z.B. einen Pinselstrich (vgl Figure 1, Seite 3). 
# 
# 
# <img src="jupyterbilder/11doku.jpg">
# 
# Die Gewichtung dieser Layer wird duch __wl__ geregelt. Es gibt in dem Paper keine genauen Angaben zu __wl__ , nur dass die Gewichtung zusammen 1 ergeben soll. Aufgrund von vielen Versuchen, wurde hier das Verhältnis<br> __wl = [0.1,0.1,0.1,0.1,0.6]__ genutzt, weil es die besten Ergebnisse geliefert hat.
# 
# 
# In __style_features__ sind alle Informationen enthalten, die der Output der 5 Layer enthält, nachdem man das Style Image durch VGG19 geschickt hat. Man benötigt auch die Shapes der Outputs, weil man __N__ und __M__ benötigt, wobei __N__ und __M__ die Breite und Höhe des Outputs beschreiben.
# 
# __GListe__ ist eine Konstante (Liste) und enthält die Gram-Matritzen von den style_features der 5 ausgewählten Layer.<br>
# __AListe__ enthält ebenfalls die Gram-Matritzen von den style_features der 5 ausgewählten Layer, allerdings sind diese nicht konstant, sondern passen sich so an, dass der Loss geringer wird.
# 
#  
# In $E_{ \text{l}}$ wird vor der Summierung eine Normalisierung vorgenommen<br>
# 
# $E_{ \text{l}} = \frac{1}{4N^{(2)}_{ \text{l}} M^{(2)}_{ \text{l}}}\sum _{ \text{i,j}} (G^{(l)}_{ \text{i,j}} - A^{(l)}_{ \text{i,j}})^2 $ <br>
# 
# $L_{Style}(w_{ \text{l}}, E_{ \text{l}}) =  \sum _{ \text{l=0}}^L (w_{ \text{l}}, E_{ \text{l}}) $ <br>
# 
# 
# 
# 
# __siehe__ [1]Seite 11

# In[10]:


#GListe=Gram-Matritzen Style Image (als Liste)
#AListe=Gram-Matritzen Generated Image (als Liste)
#wl=Gewichtung der Style_output_layer
#style_features=output der Layer


def lossstyle(GListe, AListe, style_features, wl):

    s = [i.shape for i in style_features]#shapes der outputs werden gebraucht für N,M
    lossst=0
    assert len(GListe)==len(AListe), "Länge der beiden Listen sind verschieden"
                
    zipl=zip(GListe, AListe,wl,s)
    for gm,am,wi,si in zipl:
        
        N=si[1]
        M=si[2]
        with tf.variable_scope('lossstyle'):
            E =  tf.multiply((1.0 / (4 *(N**2 * M**2 ))),tf.reduce_sum(tf.square(tf.subtract(gm, am))))
            lossst = lossst + wi*E
         
    return lossst


# ## 2.5. Total Loss und Style Transfer
# Beide Loss-Funktionen werden jetzt zusammengesetzt. Es gibt 2 Hyperparameter:<br>
# $\alpha$ , je jöher $\alpha$, umso mehr Content hat das Generated Image<br>
# $\beta$ , je höher $\beta$ ist, umso mehr Style enthält das Ergebnis
# 
# Es wird das Verhältnis $\alpha$ / $\beta$ = $1$ x $10^{(-3)}$ oder $1$ x $10^{(-4)}$ empfohlen <br>
# Der Style spielt also eine wichtigere Rolle.
# 
# __siehe__ [1]Seite 12 "The ratio α/β was either 1×10−3..."
# 
# 
# $L_{total}() = \alpha J_{content}() + \beta J_{style}()$
# 
# 
# __siehe__ [1]Seite 12 Formel 7
# 
# 
# Jetzt wird der Graph mit der Session aufgebaut. Außerdem wird hier der Adam-Optimizer implementiert. 

# In[11]:


def styletransfer(content_image,
                  style_image,
                  iterationen,
                  alpha,
                  beta,
                  learning_rate=learning_rate,
                  content_output_layer=content_output_layer,
                  style_output_layer=style_output_layer,
                  wl=wl,
                  generated_image_ref=None,
                  save_generated=None):
    content_image   = load_image(content_image)
    style_image     = load_image(style_image)
    
    GListe=[]#GListe ist eine Konstante (Liste), enthält Gram-Matrix von den style_features

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        sess.run(vgg['input'].assign(content_image))
        output_content = [vgg[s] for s in content_output_layer]
        content_features = sess.run(output_content[0]) #content_features ist eine Konstante (Liste mit nur einem Eintrag)

        sess.run(vgg['input'].assign(style_image))
        output_style = [vgg[s] for s in style_output_layer]
        style_features = sess.run(output_style) 

        for u in range(len(style_features)):
            G = sess.run(grammatrix(style_features[u],p="tensorflow"))
            GListe.append(G)

    lossc = losscontent(content_features, output_content[0])
    
    AListe=[]
    
    for a in range(len(output_style)):
        A = grammatrix(output_style[a],p="tensorflow")
        AListe.append(A)
        
    losss = lossstyle (GListe, AListe,style_features,wl)
    losstotal = tf.add(tf.multiply(float(alpha), lossc) , tf.multiply(float(beta), losss))
    opt      = tf.train.AdamOptimizer(learning_rate)
    minimize = opt.minimize(losstotal)

    with tf.Session() as sess:    
        sess.run(tf.global_variables_initializer())
        
        generated_img = content_image 

        sess.run(vgg['input'].assign(generated_img))

        bestimg=None
        bestloss=10**31
        iarray=[] #für Plot (Bokeh)
        lossarray=[] #für Plot (Bokeh)
        lossarraystyle=[] #für Plot (Bokeh)
        lossarraycontent=[] #für Plot (Bokeh)

        for i in range(iterationen):
            sess.run(minimize)
            losst = sess.run(losstotal)
            lossarray.append(losst)
            lossarraystyle.append(sess.run(losss)*beta)
            lossarraycontent.append(sess.run(lossc)*alpha)
            iarray.append(i)
            if i%500==0:
                print("Iteration ", i)
                print(losst)
                

            if bestloss>losst:
                bestloss=losst
                bestimg=sess.run(vgg['input'])
                
            if generated_image_ref is not None:
                generated_image_ref = bestimg
            
            if save_generated is not None:
                if i%100 == 0:
                    sg = np.clip(bestimg[0,:,:,:], 0, 255).astype('uint8')
                    print(sg)
                    sg_image = Image.fromarray(sg, mode="RGB")
                    sg_image.save(str(save_generated))
                    
        #Tensorboard wird hier automatisch erstellt, Bild wird unten gezeigt
        #Wichtiger Link: https://www.tensorflow.org/guide/summaries_and_tensorboard
        writer = tf.summary.FileWriter("/home/uo002/DeepStyle/jupyterbilder/tensorboard/tensorboardgatys")
        writer.add_graph(sess.graph)

    return bestimg,iarray,lossarray,lossarraystyle,lossarraycontent
        


# In[12]:


if __name__ == "__main__":
    paraliste = []



    
    paraliste.append({"content_image":"jupyterbilder/cmarokko.jpg","style_image":"jupyterbilder/svangogh2.jpg",
                      "iterationen":1000,"alpha":1,"beta":10**3,"learning_rate":1.5,
                      "content_output_layer":['conv4_2'],
                     "style_output_layer":['conv1_1','conv2_1','conv3_1','conv4_1','conv5_1'],
                     "wl":[0.1,0.1,0.1,0.1,0.6]})


    for p in paraliste:
        start = time.time()
        bestimg,iarray,lossarray,lossarraystyle,lossarraycontent = styletransfer(p["content_image"],p["style_image"],
                                                                                 p["iterationen"],p["alpha"],p["beta"],
                                                                                 learning_rate=p["learning_rate"],
                                                                                 content_output_layer=p["content_output_layer"],
                                                                                 style_output_layer=p["style_output_layer"],
                                                                                 wl=p["wl"])

        end = time.time()
        print("Zeit in Sekunden: ",end - start)
        p["Zeit"] = end - start
        p["bestimg"] = bestimg
        p["iarray"] = iarray
        p["lossarray"] = lossarray


# In[13]:


from copy import deepcopy


def load_image2(pfad):
    image = Image.open(pfad)
    image = image.convert(mode="RGB")
    image = image.resize((image_dim[2],image_dim[1]), Image.ANTIALIAS)
    image = K_image.img_to_array(image)
    image = np.clip(image, 0, 255).astype('uint8')
    return image

def show_result(p):
    ci = p["content_image"]
    ci = load_image2(ci)
    si = p["style_image"]
    si = load_image2(si)
    gi = p["bestimg"]
    gi = deepcopy(gi[0])
    gi = np.clip(gi, 0, 255).astype('uint8')

    plt.figure(1,figsize=(15,15))
    plt.subplot(131)
    plt.title("content image")
    plt.imshow(ci)
    

    plt.subplot(132)
    plt.title("style image")
    plt.imshow(si)
    
    
    plt.subplot(133)
    plt.title("generated image")
    plt.imshow(gi)
 
    plt.show()
    
if __name__ == "__main__":    
    show_result(paraliste[0])


# In[14]:


#Plot Loss-Funktion

if __name__ == "__main__":
    import bokeh
    from bokeh.io import show,output_notebook
    from bokeh.plotting import figure, output_file, ColumnDataSource
    output_notebook()
    
if __name__ == "__main__":
    p = figure(plot_width=600, plot_height=400,title="Loss-Funktionen")
    p.xaxis.axis_label = 'Iteration'
    p.yaxis.axis_label = 'Loss'
    p.line(iarray,lossarray, legend="Totalloss",line_width=2)
    p.line(iarray,lossarraystyle, line_width=2, legend="Styleloss",color="red")
    p.line(iarray,lossarraycontent, line_width=2, legend="Contentloss", color="yellow")

    show(p)


# ## 2.6. Tensorboard des Graphen:
# Tensorflow bietet die Möglichkeit, den aufgebauten Graphen als png automatisiert zu erstellen:
# <img src="jupyterbilder/tensorboard1.jpg">
# <img src="jupyterbilder/tensorboard2.jpg">

# ## 2.7. Bildliche Darstellung des gesamten Ablaufs:
# <img src="jupyterbilder/netz.jpg">
# 

# ## 2.8. Genaue Beobachtungen:
# <img src="jupyterbilder/gvergleich.jpg">
# 

# Das Projekt DeepStyle wird in dem Jupyternotebook "DeepStyleJohnson" fortgesetzt.
